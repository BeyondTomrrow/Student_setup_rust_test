extern crate rand;
extern crate app_dirs2;
extern crate preferences;
extern crate rustc_serialize;
use std::io;
use rand::Rng;
use std::time::Duration;
use std::thread;

use app_dirs2::{AppDataType, AppInfo, app_root, get_app_root};

const APP_INFO: AppInfo = AppInfo {
    name: "UserManagement",
    author: "Anthony Callahan",
};

struct User {

        username: String,
        email: String,
        user_id: u64,
        active: bool,
}

// We use a struct to define a common object this should make generating 
//new users easier and give us the ability to use a database in the future
//Consider moving all Users, input, and menu to their own files. 
// Consider using a TUI crate. 


fn main() {

    println!("{:?}", get_app_root(AppDataType::UserConfig, &APP_INFO));
    println!("{:?}", app_root(AppDataType::UserConfig, &APP_INFO));

    let mut User = User {
        email: String::from("example@example.com"),
        username: String::from("example"),
        user_id: 0,
        active: true
    };

    main_menu();

    let mut menu_entry = String::new();

    io::stdin().read_line(&mut menu_entry)
        .expect("Failed to read line");

    let menu_entry: u32 = menu_entry.trim().parse()
        .expect("Please select an option");

    // test input println!("You entered {}", menu_entry);



    if menu_entry == 1 {
        println!("You are now generating a new ID for your profile.");
        println!("Generating ID please be patient...");
        //get the user id and assign the randomly generated value to it.
        User.user_id = rand::thread_rng().gen_range(1, 100001);
        

        thread::sleep(Duration::from_secs(2));
        
        println!("Here is some basic profile information {}", User.user_id);

        
        // give a go back option and load the main menu again 
        // Split this up into multiple functions 
        // probably should right data to a database or a text file so we can use this again without generating a new ID every time
        // I dont know how to do that though so that will be soon. 

        main_menu(); // for now we will just go to the main menu to make sure rest of the data is accessible and changeable.
    
        // read_user_id(user.user_id);

    } else if menu_entry == 2 {
        if User.email.is_empty() {
            create_user_profile(User.email, User.username)
        } else {
            view_user_profile(User.username, User.email, User.user_id);
        }
    }




}

fn view_user_profile(username: String, email: String, user_id: u64) {
    println!("your username is: {}{}", username, user_id);
}

fn create_user_profile(email: String, username: String) {
    println!("You are now creating your profile.");

} 

/* fn read_user_id(id: u64) { I used this function to test passing a variable to a function will leave this as a 
    comment incase I forget please let me know if there is a better way of doing this. 

    //simple testing function 
    println!("Your User ID is: {} do not lose this.", id);

} */

fn main_menu() {
    println!(" Welcome this is a simple  management system.");
    println!(" To get started please select an option below.");
    println!(" 1. Get a ID Number.\n 2. View/Manage Profile\n 3. Exit program\n");
    //handle_user_input();
    
}

