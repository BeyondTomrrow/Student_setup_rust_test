# Management_ID thing.

This is a Management system of some sorts. Not really sure what it is, but if you have a use for it use it or add or take away I dont really care just follow the license. 

[![Build Status](https://gitlab.com/BeyondTomrrow/Student_setup_rust_test/badges/master/pipeline.svg)](https://gitlab.com/BeyondTomrrow/Student_setup_rust_test/commits/master)
## Getting Started

Run  ```cargo run```  in terminal or command prompt or whatever you use. seriously its pretty simple.

### Prerequisites

Nothing.

### Installing

Run  ```cargo run```  in terminal or command prompt or whatever you use. seriously its pretty simple.

## Deployment

Run  ```cargo run```  in terminal or command prompt or whatever you use. seriously its pretty simple.

## Built With

* [Rust](https://www.rust-lang.org/en-US/) - Programming language
* [Crates](https://crates.io/) - Dependency Management
* [RAND](https://crates.io/crates/rand) - RNG Crate used for assigning ID's to the user.

## Contributing
 Please read the contribution guide. I don't really know why you would contribute anything though This program has no use and probably never will.



## Authors

* **Anthony Callahan** 

